package Two

/**
  * @author JonaK
  * @since 8:00 PM, 7/23/2016
  */
object ProblemTwo
{

	def main(args: Array[String]): Unit =
	{
		evenFibNumSum(4000000)
	}

	def evenFibNumSum(topLimit: Int): Unit =
	{

		var lastNum = 0
		var thisNum = 1
		var sum = 0

		while (thisNum < topLimit) {
			if (thisNum % 2 == 0) {
				sum += thisNum
				println(thisNum)
			}
			val x = thisNum
			thisNum = lastNum + thisNum
			lastNum = x
		}

		println("--- " + sum)

	}
}