package Nine

import scala.collection.mutable.ListBuffer

/**
  * @author JonaK
  * @since 8:06 PM, 7/23/2016
  */
object ProblemNine
{
	def main(args: Array[String]): Unit =
	{

		val permutationList = new ListBuffer[(Int, Int)]()

		for (a <- 0 to 1000 by 1)
		{
			for (b <- 0 to 1000 by 1)
			{
				val x: (Int, Int) = (a, b)
				permutationList += x
			}
		}

		permutationList.toList.foreach(x =>
		{
			val a = x._1
			val b = x._2
			val c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2))

			if (Math.floor(c) == c)
			{
				if (a < b && b < c)
				{
					println(a + "/" + b + "/" + c + "=>" + (a + b + c))
					if (a + b + c == 1000)
					{
						println("ANSWER = " + (a*b*c))
						return
					}
				}
			}
		})
	}
}
