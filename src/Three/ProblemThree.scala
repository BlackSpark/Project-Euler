package Three

import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._

/**
  * @author JonaK
  * @since 8:01 PM, 7/23/2016
  */
object ProblemThree
{
	def main(args: Array[String]): Unit =
	{
		val num = 600851475143L
		//println(List.range(1L, num + 1L).filter(x => num % x == 0).filter(isPrime))
		val primeFactors = getFactors(num)
		println(primeFactors)
		println(primeFactors.sortBy(x => -x).headOption.get)
	}

	def isPrime(num: Long): Boolean =
	{
		if (num < 3) return true
		else if (num % 2 == 0) return false
		else {
			var i: Long = 3L
			while (i < num) {
				if (num % i == 0) return false
				if (i % 10000000 == 0) println("PFC - " + (i.toFloat / num) + "%")
				i += 2L
			}
		}
		true
	}

	def getNextPrime(num: Int): Int =
	{
		if(isPrime(num + 1)) num + 1
		else getNextPrime(num + 1)
	}

	def getFactors(num: Long): List[Long] =
	{
		var listToReturn = new ListBuffer[Long]()
		val defaultFactor = 2
		var currentFactor = 2
		var currentValue = num
		var i = 0L
		breakable {
			while(true) {
				if(currentValue == currentFactor) {
					listToReturn += currentFactor
					break
				}
				else if (currentValue % currentFactor == 0) {
					listToReturn += currentFactor
					currentValue = currentValue / currentFactor
					currentFactor = defaultFactor
				}
				else {
					currentFactor = getNextPrime(currentFactor)
				}
				i += 1L
			}
		}
		listToReturn.toList
	}
}
