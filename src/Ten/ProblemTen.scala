package Ten

/**
  * @author JonaK
  * @since 8:07 PM, 7/23/2016
  */
object ProblemTen
{

	val totalMax = 2000000
	val bootstrapMax = 500000

	def main(args: Array[String]): Unit =
	{
		var total: Long = 0
		val firstPrimes = List.range(2, bootstrapMax).filter(isPrime(_, bootstrapMax))
		println(firstPrimes.length)
		val primeList = List.range(2, totalMax).filter(findModulos(firstPrimes, _))
		println(primeList.length)
		primeList.filter(isPrime(_, totalMax)).foreach(total += _)
		firstPrimes.foreach(total += _)
		println(total)
	}

	def findModulos(list: List[Int], num: Int): Boolean =
	{
		for(i <- list){
			if(num % i == 0){
				//println(num + " % " + i + " = 0")
				return false
			}
		}
		return true
	}

	def isPrime(num: Int, max: Int): Boolean =
	{
		println("~" + num/max.toDouble*100 + "%")
		if(num <= 1) return false
		if (num < 3) return true
		else if (num % 2 == 0) return false
		else
		{
			var i: Int = 3
			while (i < num)
			{
				if (num % i == 0) return false
				i += 2
			}
		}
		true
	}
}
