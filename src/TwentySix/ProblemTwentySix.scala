package TwentySix

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._;

/**
  * @author JonaK (so far)
  * @since 2:59 AM, 8/1/2016
  */
class ThingHolder(val listToNotIterateThrough: ListBuffer[Int], val possibleIrrationals: ListBuffer[Int], val maxLength: Int, val maxLengthNum: Int)
{}

object ProblemTwentySix
{
    
    val min = 1;
    val max = 1000;
    val listOfDeciNumbers = new mutable.HashMap[Int, List[Int]];
    
    def main(args: Array[String]): Unit =
    {
        val listToNotIterateThrough = new ListBuffer[Int];
        (1 to max).foreach(x => if (getDeciNumber(x, 1000) == -1) listToNotIterateThrough += x);
    
        val thingHolder = doTheThing((1 to max).diff(listToNotIterateThrough).toList, 1000, 100, 100);
        
        val possibleIrrationals = thingHolder.possibleIrrationals.toList;
        val thingHolderHC = doTheThing(possibleIrrationals, 10000, 1000, 1000)
        
        println("NORMAL");
        println(thingHolder.possibleIrrationals);
        println(thingHolder.maxLength);
        println(thingHolder.maxLengthNum);
        println();
        println("Possible Irrationals -");
        println(thingHolderHC.possibleIrrationals);
        println(thingHolderHC.maxLength);
        println(thingHolderHC.maxLengthNum);
    }
    
    def getDeciList(num: Int, indexStart: Int, indexEnd: Int): List[Int] =
    {
        val list = new ListBuffer[Int];
        (indexStart until indexEnd).foreach(list += getDeciNumber(num, _));
        return list.toList;
    }
    
    def getDeciNumber(num: Int, index: Int): Int =
    {
        if (index <= 0) return -1
        if (listOfDeciNumbers.contains(num) && listOfDeciNumbers(num).length > index) return listOfDeciNumbers(num)(index);
        else return getLongDivNumber(num, index);
    }
    
    private def getLongDivNumber(divisor: Int, index: Int): Int =
    {
        val quotient = new ListBuffer[Int]();
        // List of numbers in the quotient, e.g. 1/2 = [0, 5], 1/20 = [0, 0, 5], etc.
        var dividend = 1; // What would currently be under the long-division-symbol-thing
        
        breakable
        {
            for (i ← 0 to index)
            {
                if (dividend == 0)
                {
                    break;
                }
                else
                {
                    var fitTimes = 0; // Number of times the divisor fits into the dividend.
                    while (divisor <= dividend)
                    {
                        dividend -= divisor;
                        fitTimes += 1;
                    }
                    dividend *= 10;
                    quotient += fitTimes;
                }
                //println(quotient.mkString(",") + "\n" + "----------------------" + "\n" + divisor + "|" + dividend + "\n\n")
            }
        }
        
        if (listOfDeciNumbers.contains(divisor))
        {
            val top = listOfDeciNumbers(divisor).length
            val savedQuotient = listOfDeciNumbers(divisor).to[ListBuffer];
            for (i ← top until quotient.length)
            {
                savedQuotient += quotient(i);
            }
            listOfDeciNumbers(divisor) = savedQuotient.toList;
        }
        else
        {
            listOfDeciNumbers += (divisor → quotient.toList);
        }
        
        if (index >= quotient.length) return -1;
        return quotient(index);
    }
    
    def doTheThing(listToIterate: List[Int], maxDistance: Int, maxTries: Int, maxFrontPorch: Int): ThingHolder =
    {
        var maxLength = 0;
        var maxLengthNum = 0;
        val listToNotIterateThrough = new ListBuffer[Int];
        val possibleIrrationals = new ListBuffer[Int];
    
        for (i <- listToIterate)
        {
            println(i);
            var frontporch = 1;
            var tries = 1;
            breakable
            {
                while (true)
                {
                    var failDistance = false;
                    val n1 = getDeciNumber(i, frontporch);
                    var ii = frontporch;
                    for (times <- 1 to tries)
                    {
                        breakable
                        {
                            do
                            {
                                ii += 1;
                                if (ii > maxDistance)
                                {
                                    failDistance = true;
                                    break;
                                }
                            }
                            while (n1 != getDeciNumber(i, ii) && n1 != getDeciNumber(i, (ii - frontporch) + ii));
                        }
                    }
                    
                    if (!failDistance)
                    {
                        val distance = ii - frontporch;
                        val a = getDeciList(i, frontporch, frontporch + distance);
                        val b = getDeciList(i, ii, ii + distance);
                        val c = getDeciList(i, ii + distance, ii + distance + distance);
                        if (a == b && b == c && a == c)
                        {
                            if (distance >= maxLength)
                            {
                                maxLength = distance;
                                maxLengthNum = i;
                                break;
                            }
                            else
                            {
                                listToNotIterateThrough += i;
                                break;
                            }
                        }
                        else
                        {
                            tries += 1;
                        }
                    }
                    else tries = maxTries + 1;
                    
                    if (tries > maxTries)
                    {
                        frontporch += 1;
                        tries = 1;
                    }
                    if (frontporch > maxFrontPorch)
                    {
                        possibleIrrationals += i;
                        break;
                    }
                }
            }
        }
        
        return new ThingHolder(listToNotIterateThrough, possibleIrrationals, maxLength, maxLengthNum);
        
    }
    
}
