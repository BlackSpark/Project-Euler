package EulerLibrary

import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Various methods useful for Project Euler problems.
  *
  * @author JonaK
  * @since 10:04 AM, 10/4/2016
  */
object EulerLibrary
{
    /**
      * Caches factors of numbers.
      */
    var factorCache = new mutable.HashMap[Int, Set[Int]]();
    
    /**
      * Caches numbers and whether or not they're prime.
      */
    var primeCache = new mutable.HashMap[Int, Boolean]();
    
    /**
      * A testing function for this library.
      *
      * @param args Command line arguments (unused).
      */
    def main(args: Array[String]): Unit =
    {
        pregeneratePrimes(10000);
        println(isPrime(993));
    }
    
    /**
      * Resets both caches.
      */
    def reset(): Unit =
    {
        factorCache = new mutable.HashMap[Int, Set[Int]]();
        primeCache = new mutable.HashMap[Int, Boolean]();
    }
    
    /**
      * Pregenerates the prime number cache to a maximum number.
      *
      * @param max The maximum value to pregenerate to.
      */
    def pregeneratePrimes(max: Int): Unit =
    {
        getNextPrime(max, back = true);
    }
    
    /**
      * Gets all the factors of a number, excluding that number.
      *
      * @param num The number to factorize.
      * @return A set of factors of that number (excluding that number).
      */
    def getProperFactors(num: Int): Set[Int] =
    {
        val allFactors = getFactors(num);
        return allFactors.diff(Set(num));
    }
    
    /**
      * Gets all the factors of a number, including that number.
      *
      * @param num The number to factorize.
      * @return A set of factors for the given number (including that number).
      */
    def getFactors(num: Int): Set[Int] =
    {
        val primeFactors = getPrimeFactors(num);
        val factorBuffer = new ListBuffer[Int];
        factorBuffer += 1;
        for (i <- 1 to primeFactors.length)
        {
            primeFactors.combinations(i).toList.map(_.product).foreach(factorBuffer += _);
        }
        return factorBuffer.toSet;
    }
    
    /**
      * Gets the prime factorization of a number.
      *
      * @param num The number to factorize.
      * @return A sequence of integers representing the prime factors
      */
    @tailrec
    def getPrimeFactors(num: Int, currentFactors: Seq[Int] = Seq()): Seq[Int] =
    {
        if (isPrime(num))
        {
            return Seq(num) ++ currentFactors;
        }
        var factor = 2;
        
        while (num % factor != 0)
        {
            factor = getNextPrime(factor);
        }
        return getPrimeFactors(num / factor, currentFactors ++ Seq(factor));
    }
    
    /**
      * Determines if the given number is prime, using caches, checking for negative numbers, and checking for modulo 2.
      *
      * @param num The number to be checked.
      * @return True if prime, false otherwise.
      */
    def isPrime(num: Int): Boolean =
    {
        if (num <= 0) return false;
        else if (num > 0 && num < 3) return true;
        else if (num % 2 == 0) return false;
        else if (primeCache.contains(num)) return primeCache(num);
        else return findPrime(num);
    }
    
    /**
      * Gets the nearest prime number in a direction
      *
      * @param start Starting number
      * @param back  True if looking backwards
      * @return The next prime number.
      */
    private[this] def getNextPrime(start: Int, back: Boolean = false): Int =
    {
        var currentNum = if (back) start - 1 else start + 1;
        while (!isPrime(currentNum))
        {
            if (back)
                currentNum -= 1;
            else
                currentNum += 1;
        }
        return currentNum;
    }
    
    /**
      * Determines if a number is prime. Makes no checks before starting on the algorithm.
      * Adds to the prime cache.
      *
      * @param num The number to be checked.
      * @return True if prime, false otherwise.
      */
    private[this] def findPrime(num: Int): Boolean =
    {
        var skipNums = new ListBuffer[Int];
        val lastNum = getNextNum(num);
        var i = 0;
        while (lastNum(i) <= num)
        {
            if (skipNums.contains(num))
            {
                primeCache += num -> false;
                return false;
            }
            val mtpls = getMultiplesWithCache(lastNum(i), num);
            skipNums ++= mtpls;
            i += 1;
        }
        return true;
    }
    
    /**
      * Public-facing convenience method for [[getMultiplesWithOptions]]. Forces the cache-adding process off.
      *
      * @param n   Number to find multiples of.
      * @param max Number around which to cease finding multiples.
      * @param strict True will search slightly beyond max.
      * @return Sequence of distinct multiples.
      */
    def getMultiples(n: Int, max: Int, strict: Boolean): Seq[Int] = getMultiplesWithOptions(n, max, strict, addCache = false);
    
    /**
      * Convenience Method for [[getMultiplesWithOptions]]
      *
      * @param n    Number to find multiples of
      * @param max  Numer around which to cease finding multiples
      * @return     Sequence of distinct multiples.
      */
    private[this] def getMultiplesWithCache(n: Int, max: Int): Seq[Int] = getMultiplesWithOptions(n, max, strict = false, addCache = true);
    
    /**
      * Determines the multiples of a number up to around max. Adds these multiples and the number to be found to the cache.
      *
      * @param n   Number to find multiples of.
      * @param max Number around which to cease finding multiples.
      * @param strict True will search slightly beyond max.
      * @param addCache True will enable prime cache-adding.
      * @return Sequence of distinct multiples.
      */
    private[this] def getMultiplesWithOptions(n: Int, max: Int, strict: Boolean = true, addCache: Boolean = true): Seq[Int] =
    {
        if (addCache)
            primeCache += n -> true
        val multiplelb = new ListBuffer[Int];
        val top = if(strict) max / n else max / n + 1;
        for (x <- 2 to top)
        {
            val m = n * x;
            multiplelb += m;
            if (addCache)
                primeCache += m -> false;
        }
        return multiplelb.distinct;
    }
    
    /**
      * Generates the next acceptable number to check in the [[EulerLibrary.isPrime]] method.
      *
      * The current state is held externally. and passed into the mathod each time it is run.
      *
      * @param num   The number we're finding acceptable numbers to check with.
      * @param x     The current held number.
      * @param i     The current iterator.
      * @return      A stream of Ints to check.
      */
    private[this] def getNextNum(num: Int, x: Int = 1, i: Int = 2): Stream[Int] =
    {
        val combined = x + i;
        if (!primeCache.contains(combined))
        {
            return combined #:: getNextNum(num, combined, 2);
        }
        else if (!primeCache(combined))
        {
            return getNextNum(num, x, i + 2)
        }
        else
        {
            return combined #:: getNextNum(num, combined, 2);
        }
    }
}
