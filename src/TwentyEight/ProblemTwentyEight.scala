package TwentyEight

/**
  * @author JonaK
  * @since 6:44 PM, 9/3/2016
  */
object ProblemTwentyEight
{
    def main(args: Array[String]): Unit =
    {
        val spiralsize = 1001;
        val maxSpiralNum = spiralsize * spiralsize;
        val spiral = Array.ofDim[Int](spiralsize, spiralsize);
        var x = (spiralsize - 1) / 2;
        var y = (spiralsize - 1) / 2;
        spiral(y)(x) = 1;
        /*         +1
        *      -1
        *         -1
        *         -1
        *      +1
        *      +1
        *         +1
        *         +1
        *         +1
        *      -1
        *      -1
        *      -1
        *         -1
        *         -1
        *         -1
        *         -1
        *      +1
        *      +1
        *      +1
        *      +1
        *         +1
        *         +1
        *         +1
        *         +1
        *         +1
        *       ...
        */
        var pos = true;
        var doX = true;
        var signcount = 1;
        var cNum = 2;
        
        var times = 1;
        var cTimes = 0;
        
        while (cNum <= maxSpiralNum)
        {
            // -- Inc
            if (pos)
            {
                if (doX) x += 1;
                else y -= 1;
                
            }
            else
            {
                if (doX) x -= 1;
                else y += 1;
            } // -- Inc
            spiral(y)(x) = cNum;
            cTimes += 1;
            cNum += 1;
            
            if (times == cTimes)
            {
                signcount += 1;
                cTimes = 0;
                if (!doX)
                {
                    times += 1;
                }
                doX = !doX;
                if (signcount == 2)
                {
                    signcount = 0;
                    pos = !pos;
                }
            }
            
        }
        
        var total = -1;
        
        for (k <- 0 until spiralsize)
        {
            total += spiral(k)(k);
            val j = spiralsize - 1 - k;
            total += spiral(k)(j);
        }
        println(total);
    }
}
