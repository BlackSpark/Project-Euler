package Thirty

import scala.collection.mutable.ListBuffer


/**
  * @author JonaK
  * @since 10:50 AM, 10/14/2016
  */
object ProblemThirty
{
    
    def main(args: Array[String]): Unit =
    {
        val selPower = 5;
        var numList = new ListBuffer[Int];
        for(num <- 10000 to 99999)
        {
            val powerSum = splitNumber(num).map(Math.pow(_, selPower).toInt).sum;
            if(powerSum == num) numList += num;
        }
        
        println(numList);
        println(numList.sum);
    }
    
    def splitNumber(num: Int): List[Int] = num.toString.toList.map(_.toString.toInt);
    
}
