package Five

/**
  * @author JonaK
  * @since 8:04 PM, 7/23/2016
  */
object ProblemFive
{
	def main( args: Array[ String ] ): Unit =
	{
		var i = 1

		while (true)
		{
			var valid = true
			Iterator.range(1, 20).foreach(x => if(i % x != 0) valid = false)
			if(valid)
			{
				println(i)
				return
			}
			i += 1
		}
	}
}
