package Six

/**
  * @author JonaK
  * @since 8:04 PM, 7/23/2016
  */
object ProblemSix
{
	def main( args: Array[ String ] ): Unit =
	{
		var ssum = 0D
		(1 to 100).foreach(ssum += Math.pow(_, 2))
		var tssum = 0D
		(1 to 100).foreach(tssum += _)
		tssum = Math.pow(tssum, 2)

		println(BigDecimal(tssum - ssum).formatted("%1$.3f"))

	}
}
