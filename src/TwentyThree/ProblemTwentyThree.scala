package TwentyThree

import scala.collection.mutable.ListBuffer;
import EulerLibrary.EulerLibrary;

/**
  * @author JonaK
  * @since 2:28 PM, 7/30/2016
  */
object ProblemTwentyThree
{

	val min = 12;
	val max = 28123;
	val abundantNumbers = new ListBuffer[Int];
    
	def main(args: Array[String]): Unit =
	{
        EulerLibrary.pregeneratePrimes(max);
		var total = 0;
		for (i <- min to max)
		{
			if (isAbundant(i))
			{
				abundantNumbers += i;
				println(i);
			}
		}
		val abundantSums = composeSums();

		val notSums = (1 to max).toList.diff(abundantSums);
        println("----- POSSIBLE SUMS >= 28123 -----");
        println(abundantSums);
		println("----- NOT SUMMABLE -----");
		println(notSums);
        println("----- SUM OF NOT SUMMABLE -----");
		println(notSums.sum);
	}

	def composeSums(): Seq[Int] =
	{
        var abundantSums = new ListBuffer[Int];
        
        for (k <- abundantNumbers)
		{
			for (j <- abundantNumbers)
			{
				if(k + j <= max) abundantSums += k + j;
			}
			println(k.toDouble * 100 / abundantNumbers.max + "%");
		}
        
        println("Distinctifying");
        
        return abundantSums.distinct;
	}

	def isAbundant(num: Int): Boolean = EulerLibrary.getProperFactors(num).sum > num;
	
}
