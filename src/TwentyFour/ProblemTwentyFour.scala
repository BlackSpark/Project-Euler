package TwentyFour

/**
  * @author JonaK
  * @since 4:40 PM, 7/30/2016
  */
object ProblemTwentyFour
{
	def main(args: Array[String]): Unit =
	{
		bruteForce();
	}
	def bruteForce(): Unit =
	{
		val nums = List(0,1,2,3,4,5,6,7,8,9);
		val perms = nums.permutations.toList.map(l => l.mkString("")).sorted;
		print(perms(1000000 % perms.length - 1));
	}
}
