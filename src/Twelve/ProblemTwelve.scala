package Twelve

import scala.collection.mutable.ListBuffer

/**
  * @author JonaK
  * @since 1:04 PM, 7/24/2016
  */
object ProblemTwelve
{
	def main(args: Array[String]): Unit =
	{
		var i = 2
		while (true)
		{
			val trinum = getTriangularNumber(i)
			val numDivis = getDivisors(trinum).length
			if (numDivis >= 300)
			{
				println("Triangular Number " + trinum + " (#" + i + ") has " + numDivis + " divisors")
				if(numDivis >= 500) return
			}
			i += 1
		}
	}

	def getTriangularNumber(index: Int): Int = (1 to index).sum

	def getDivisors(num: Int): List[Int] =
	{
		var possibleDivisors = new ListBuffer[Int]
		(1 to Math.ceil(Math.sqrt(num)).toInt).foreach(possibleDivisors += _)
		var divisors = new ListBuffer[Int]
		var iterator = 0

		while (iterator < possibleDivisors.length)
		{
			val div = num / possibleDivisors(iterator).toDouble
			if (Math.floor(div) == div)
			{
				divisors += div.toInt
				divisors += possibleDivisors(iterator)
			}

			iterator += 1
		}

		return divisors.toList
	}
}
