package TwentyOne

import scala.collection.mutable.ListBuffer

/**
  * @author JonaK
  * @since 11:43 AM, 7/30/2016
  */
object ProblemTwentyOne
{
	def main(args: Array[String]): Unit =
	{
		var amicablenums = new ListBuffer[Int]();
		for (a <- 1 until 10000)
		{
			val b = getProperDivisors(a).sum;
			val shouldBeA = getProperDivisors(b).sum;
			if (a == shouldBeA && a != b && !amicablenums.contains(a) && !amicablenums.contains(b))
			{
				amicablenums += a;
				amicablenums += b;
			}
		}

		println(amicablenums.sum);
	}

	def getProperDivisors(num: Int): List[Int] =
	{
		var possibleDivisors = new ListBuffer[Int]
		(1 to Math.ceil(Math.sqrt(num)).toInt).foreach(possibleDivisors += _)
		var divisors = new ListBuffer[Int]
		var iterator = 0

		while (iterator < possibleDivisors.length)
		{
			val div = num / possibleDivisors(iterator).toDouble
			if (Math.floor(div) == div)
			{
				divisors += div.toInt
				divisors += possibleDivisors(iterator)
			}

			iterator += 1
		}

		divisors = divisors.filterNot(_ == num);

		return divisors.toList
	}
}
