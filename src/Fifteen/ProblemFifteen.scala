package Fifteen

import scala.collection.mutable.ListBuffer

/**
  * @author JonaK
  * @since 1:26 PM, 7/25/2016
  */
object ProblemFifteen
{
	val latticeSize = 20;

	def main(args: Array[String]): Unit =
	{
		smartAlgorithm();
	}

	def smartAlgorithm(): Unit =
	{
		val size = latticeSize + 1;
		var lattice = Array.ofDim[Long](size, size);

		for (i <- 0 until size)
		{
			lattice(size - 1)(i) = 1;
			lattice(i)(size - 1) = 1;
		}

		latticePrinter(lattice);

		for (i <- (size - 2) to 0 by -1)
		{
			for (j <- (size - 2) to 0 by -1)
			{
				lattice(i)(j) = lattice(i)(j + 1) + lattice(i + 1)(j);
			}
		}

		latticePrinter(lattice);

		println(lattice(0)(0));
	}

	def latticePrinter(lattice: Array[Array[Long]]): Unit =
	{
		var a = new StringBuilder();

		for (arr <- lattice)
		{
			for (i <- arr)
			{
				a ++= i + " ";
			}
			a ++= "\n";
		}
		println(a.toString);
	}

	def bruteForce(): Unit =
	{
		var latticeOptions = new ListBuffer[Int];
		for (i <- 0 until latticeSize)
		{
			latticeOptions += 0
			latticeOptions += 1
		}
		println(latticeOptions.permutations.length)
	}
}
