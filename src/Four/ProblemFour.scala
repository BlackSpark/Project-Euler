package Four

import scala.collection.mutable.ListBuffer

/**
  * @author JonaK
  * @since 8:03 PM, 7/23/2016
  */
object ProblemFour
{
	def main( args: Array[ String ] ): Unit =
	{
		val palindromeList = new ListBuffer[Int]()
		for (i <- 999 to 1 by -1)
		{
			for (ii <- 999 to 1 by -1)
			{
				val productString = (i * ii).toString
				println(i + "*" + ii + "=" + productString)
				if (productString.length % 2 == 0)
				{
					val s1 = productString.substring(0, productString.length / 2)
					val s2 = productString.substring(productString.length / 2, productString.length).reverse
					if (s1.equals(s2)) {
						println("\t => " + productString)
						palindromeList += (i * ii)
					}
				}
			}
		}
		println(palindromeList.toList.sorted.reverse.head)
	}
}

