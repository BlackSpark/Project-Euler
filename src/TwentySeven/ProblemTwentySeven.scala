package TwentySeven

import EulerLibrary.EulerLibrary.{pregeneratePrimes, isPrime};

/**
  * Created by jonak on 10/13/2016.
  */

object ProblemTwentySeven
{
    type FnType = (Int) => Int;
    
    def main(args: Array[String]): Unit =
    {
        pregeneratePrimes(fn(999, 1000)(10));
        var maxLen = 0;
        var maxAB = (-1000, -1001);
        
        for (a <- -999 to 999; b <- (0 to 1000).filter(isPrime))
        {
            val f: FnType = fn(a, b);
            var n = 0;
            var nIsPrime = isPrime(f(n));
            while (nIsPrime)
            {
                if (n > maxLen)
                {
                    maxLen = n;
                    maxAB = (a,b);
                }
                n += 1;
                nIsPrime = isPrime(f(n));
                if(n > 200)
                    println(nIsPrime);
            }
        }
    
        printVals(fn(maxAB._1, maxAB._2), maxLen);
    
    
        println("Maximum n-val - " + maxLen);
        println("(A, B) = " + maxAB);
        println("A * B = " + maxAB._1 * maxAB._2);
    }
    
    def fn(a: Int, b: Int)(n: Int): Int = Math.pow(n, 2).toInt + a * n + b;
    
    def printVals(f: FnType, maxN: Int): Unit = (0 to maxN).foreach(n => println(f(n) + " => " + isPrime(f(n))));
    
}
