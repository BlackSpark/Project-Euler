package TwentyNine

import scala.collection.mutable;

/**
  * @author JonaK
  * @since 6:37 PM, 9/4/2016
  */
object ProblemTwentyNine
{
    
    def main(args: Array[String]): Unit =
    {
        val setOfPowers = mutable.Set[BigInt]();
        val min = 2;
        val max = 100;
        
        for (a <- min to max)
            for (b <- min to max)
                setOfPowers += bigPow(a, b);
        
        println("getting size");
        
        println(setOfPowers.toList.sorted);
        println(setOfPowers.size)
        
        //println(setOfPowers.toList.diff(ProblemTwentyNineTest2.tryThis().toList))
    }
    
    def bigPow(a: BigInt, b: BigInt): BigInt =
    {
        var total = BigInt(1);
        var it = b;
        while (!it.equals(BigInt(0)))
        {
            total = total * a;
            it = it - 1;
        }
        return total;
    }
    
}

/*
object ProblemTwentyNineTest2
{
    
    def tryThis(): Set[BigInt] =
    {
        val mapOfPowerSets = mutable.HashMap[Int, Set[BigInt]]();
        val min = 2;
        val max = 100;
        
        for (base <- (min to max).filter(isPrime))
        {
            val powerSet = mutable.Set[BigInt]();
            for (power <- min to max)
                powerSet += bigPow(base, power);
            mapOfPowerSets += base -> powerSet.toSet;
        }
        
        for (k <- mapOfPowerSets.keysIterator)
        {
            val multiples = getMultiples(k, max, strict = true);
            multiples.foreach(multiple => mapOfPowerSets += multiple -> mapOfPowerSets(k).map(bi => bi * multiple / k));
        }
        
        println("getting size");
        
        return collapse(mapOfPowerSets.toMap);
    }
    
    def bigPow(a: BigInt, b: BigInt): BigInt =
    {
        var total = BigInt(1);
        var it = b;
        while (!it.equals(BigInt(0)))
        {
            total = total * a;
            it = it - 1;
        }
        return total;
    }
    
    def collapse(in: Map[Int, Set[BigInt]]): Set[BigInt] =
    {
        var retSet = Set[BigInt]();
        for (k <- in.keysIterator)
        {
            retSet ++= in(k);
        }
        return retSet;
    }
    
}
*/