package Twenty

/**
  * @author JonaK
  * @since 11:06 AM, 7/30/2016
  */
object ProblemTwenty
{

	def main(args: Array[String]): Unit =
	{
		var bignumber = BigInt("1");
		for (i <- 2 to 100)
		{
			bignumber *= i;
		}
		val bigstring = bignumber.toString();
		println(bignumber.toString())

		var eachtotal = 0;

		for (s <- bigstring.toCharArray)
		{
			eachtotal += s.toString.toInt;
		}

		println(eachtotal);
	}

}
