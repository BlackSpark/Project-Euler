package Fourteen

/**
  * @author JonaK
  * @since 11:52 PM, 7/24/2016
  */
object ProblemFourteen
{
	val top = 1000000

	def main(args: Array[String]): Unit =
	{
		val chain = (1 to top).map(collatz)
		println(chain.max);
		println(chain.indexOf(chain.max) + 1);

	}

	def collatz(num: Int): Long =
	{
		if(num % 1000 == 0) println(num / top.toDouble * 100 + "%");
		var numIters = 0L;
		var varNum = num.toLong;
		while (varNum != 1)
		{
			if (varNum % 2 == 0)
			{
				varNum /= 2;
			}
			else
			{
				varNum = varNum * 3 + 1;
			}
			numIters += 1;
		}
		return numIters;
	}
}
