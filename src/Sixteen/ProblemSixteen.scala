package Sixteen

/**
  * @author JonaK
  * @since 1:35 PM, 7/25/2016
  */
object ProblemSixteen
{
	def main(args: Array[String]): Unit =
	{
		var dec = BigInt(2);
		for(i <- 1 until 1000)
			{
				dec *= 2;
			}
		println(dec.toString().split("").map(_.toInt).sum);
	}
}
