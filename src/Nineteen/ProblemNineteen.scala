package Nineteen

/**
  * @author JonaK
  * @since 12:37 PM, 7/26/2016
  */
object ProblemNineteen
{
	def main(args: Array[String]): Unit =
	{
		var day = 6;
		var year = 1901;
		var month = 1;
		var counter = 0;

		while (year <= 2000)
		{
			day += 7;
			val dayInfo = pastMonth(day, month, year);
			if (dayInfo._1)
			{
				if (month == 12)
				{
					month = 0;
					year += 1;
				}
				month += 1;
				day = dayInfo._2;
			}
			if (day == 1)
			{
				counter += 1;
			}
		}
		println(counter);
	}

	def pastMonth(day: Int, month: Int, year: Int): (Boolean, Int) =
	{
		val daysInMonth = month match
		{
			case 4 | 6 | 9 | 11 => 30;
			case 2 => if (year % 4 != 0 && year % 400 == 0) 28 else 29;
			case _ => 31;
		}

		if (day > daysInMonth) return (true, day - daysInMonth);
		else return (false, -1);
	}
}
