package Seventeen

import scala.collection.mutable.ListBuffer

/**
  * @author JonaK
  * @since 5:04 PM, 7/25/2016
  */
object ProblemSeventeen
{
    val top = 1000;
    
    val one = "one";
    val two = "two";
    val three = "three";
    val four = "four";
    val five = "five";
    val six = "six";
    val seven = "seven";
    val eight = "eight";
    val nine = "nine";
    val ten = "ten";
    val eleven = "eleven";
    val twelve = "twelve";
    val thirteen = "thirteen";
    val fourteen = "fourteen";
    val fifteen = "fifteen";
    val sixteen = "sixteen";
    val seventeen = "seventeen";
    val eighteen = "eighteen";
    val nineteen = "nineteen";
    val twenty = "twenty";
    val thirty = "thirty";
    val forty = "forty";
    val fifty = "fifty";
    val sixty = "sixty";
    val seventy = "seventy";
    val eighty = "eighty";
    val ninety = "ninety";
    val hundred = "hundred";
    val thousand = "thousand";
    val and = "and";
    
    val onesteens = List(null, one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen, seventeen, eighteen, nineteen);
    val tens = List(null, ten, twenty, thirty, forty, fifty, sixty, seventy, eighty, ninety);
    
    def main(args: Array[String]): Unit =
    {
        var buffer = new ListBuffer[String];
        (1 to top).foreach(i => {buffer += makeIntoString(i); println(i + " => " + makeIntoString(i))});
        
        var total = 0L;
        buffer.toList.foreach(total += _.length);;
        println(total);
    }
    
    def makeIntoString(n: Int): String =
    {
        n match
        {
            case 0 => return "";
            case x if x < 20 => return onesteens(x);
            case x if x >= 20 && x < 100 => return tens(x / 10) + makeIntoString(x - ((x / 10) * 10));
            case x if x >= 100 && x < 1000 => return onesteens(x / 100) + hundred + (if((x - ((x / 100)*100)) != 0) and else "") + makeIntoString(x - ((x / 100) * 100));
            case x if x >= 1000 => return onesteens(x / 1000) + thousand + (if((x - ((x / 1000)*1000)) != 0) and else "") + makeIntoString(x - ((x / 1000) * 1000));
        }
        return "idk";
    }
    
    
}