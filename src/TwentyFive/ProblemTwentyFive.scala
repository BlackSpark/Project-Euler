package TwentyFive

/**
  * @author JonaK
  * @since 2:52 AM, 8/1/2016
  */
object ProblemTwentyFive
{

	def main(args: Array[String]): Unit =
	{
		var n1 = BigInt(1);
		var n2 = BigInt(1);
		var i = 3;
		while(true)
			{
				val n3 = n1 + n2;
				if(n3.toString.length == 1000)
					{
						print(i);
						return;
					}
				n1 = n2;
				n2 = n3;
				i += 1;
			}
	}

}
