package One

/**
 * @author JonaK
 * @since 7:56 PM, 7/23/2016
 */

fun main(args: Array<String>) {
    natNumSum(1000)
}

fun natNumSum(topLimit: Int) {
    var sum = 0
    for (i in 1..topLimit - 1)
        if (i % 3 == 0 || i % 5 == 0)
            sum += i
    println(sum)
}