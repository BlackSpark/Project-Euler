package Seven

/**
  * @author JonaK
  * @since 8:05 PM, 7/23/2016
  */
object ProblemSeven
{
	def main(args: Array[String]): Unit =
	{
		var primeNum = 1
		var primeCount = 0
		var i = 0

		while (primeCount <= 10002)
		{
			if (isPrime(i))
			{
				primeNum = i
				primeCount += 1
			}
			i += 1
			println("~" + (primeCount.toDouble / 10001 * 100).toInt + "%")
		}

		println(primeNum)
	}

	def isPrime(num: Long): Boolean =
	{
		if (num < 3) return true
		else if (num % 2 == 0) return false
		else
		{
			var i: Long = 3L
			while (i < num)
			{
				if (num % i == 0) return false
				if (i % 10000000 == 0) println("PFC - " + (i.toFloat / num) + "%")
				i += 2L
			}
		}
		true
	}
}
